"""
Function which returns sum of two integers
"""
import logging
from utils.log_format_helpers import StructuredMessage

extra = {'app_name':'MainApp'}

def calculate_sum(x,y):
    """
    Args:
    Returns:
    """ 
    # print(__name__)
    logger = logging.getLogger(__name__)
    #to add extra constant attribute in logRecord
    # logger = logging.LoggerAdapter(logger, extra)
    msg = "calculating sum of {:.2f} and {:.2f}".format(x,y)
    logger.info(msg)
    logger.warning('calculating sum , expected int got float')
    result = float(x) + float(y)
    logger.info('calculating sum done')
    logger.warning('calculating sum done, running low on memmory')
    logger.warning('calculating sum done, running low on memmory')
    logger.critical('running low on memmory, system about to halt')
    return str(result)


def calculate_diff(x,y):
    """
    Args:
    Returns:
    """ 
    # print(__name__)
    logger = logging.getLogger(__name__)
    #to add extra constant attribute in logRecord
    # logger = logging.LoggerAdapter(logger, extra)
    msg = "calculating sum of {:.2f} and {:.2f}".format(x,y)
    logger.info(StructuredMessage(module=__name__, msg=msg))
    result = float(x) + float(y)
    logger.info(StructuredMessage(msg='Task Completed', module=__name__))
    return str(result)

