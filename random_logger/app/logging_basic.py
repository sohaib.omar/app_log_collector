"""logging basics"""


from mathfuncs.sum import calculate_sum
from utils.log_format_helpers import StructuredMessage
import logging
import logging.config
import time

def main():
    """todo:"""

    a = 10
    b = 20
    # extra = {'app_name':'MainApp'}

    # logging.basicConfig(filename='myapp.log', filemode='w', level=logging.CRITICAL, format='%(asctime)s  - %(levelname)s - %(name)s - %(message)s')
    logging.config.fileConfig('logging.conf')

    # beacause in logging python2 module default time is local time, 
    logging.Formatter.converter = time.gmtime

    logger = logging.getLogger("mathfuncs")
    
    #to add extra constant attribute in logRecord
    # logger = logging.LoggerAdapter(logger, extra)

    # logger.info("non-structurred")
    logger.info('Starting Calculation')
    logger.warning('Starting Calculation Warning')
    calculate_sum(a, b)
    logger.error('Completerd job with k errors')
    logger.error("Things were going good, now I can not connect to server -----")


if __name__ == "__main__":
    try:
        while True:
            main()
            print("\n\n-----------------------------------------------PRESS CTRL+C TO EXIT-----------------------------------------------\n\n")
            time.sleep(60)
    except KeyboardInterrupt:
        print("See Ya!")
        pass
