#! /bin/bash

#start elasticsearch pod and service
kubectl apply -f elasticsearch/elasticsearch_deployment.yaml 
kubectl apply -f elasticsearch/elasticsearch_service.yaml

# start logstash
kubectl apply -f logstash/logstash_conf.yaml
kubectl apply -f logstash/logstash_deployment.yaml
kubectl apply -f logstash/logstash_service.yaml

#start filebeat
kubectl apply -f filebeat/beat_manifest.yaml 

