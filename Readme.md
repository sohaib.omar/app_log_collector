# Log Collection using ELK stack
An app which collects logs from any application using below pipeline.
filebeat -> logstash -> elasticsearch

## Setting-up in kubernetes:
1. executing cmd **sh elk_up.sh**, will set-up all required pods and services in kubernetes cluster. 
2. To access ES server, get the port exposed  by ES pod service using **kubectl describe service elasticsearch --namespace=casb-deployment | grep NodePort** then use this port with machine IP for ex. **http://machine_ip:NodePort** to access ES server.

**Note:** This assumes that kubernetes cluster is laready setup for casb-deployment and all the apps are using latest docker images.

# View logs in Kibana
1. Set ElasticServer path in kibana conf. in path */etc/kibana/kibana.yml* and set **elasticsearch.url: "http://ES_IP:PORT"**.
2. Start kibana, setup index pattern and logs are ready to be discovered.


## Todo's
1. Health Check in kubernetes.
2. Add annotations in FileBeat(if required)

